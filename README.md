<div align="center">
    <img src="./imgs/shad.png"  width="300">
</div>

Привет!
Это учебник по машинному обучению. Он состоит из двух частей - основного курса теории, который надлежит читать подряд, и набора статей отвечающих на вопросы возникающие в реальной практике аналитика или ML-инженера. 

## Теория машинного обучения

1. [Задача ML](./chapters/Intro.md)
2. [Линейные модели и вероятностный взгляд на ML](#)
3. [Деревья решений и бустинг](#)
4. [Метрические алгоритмы](#)
5. [Матричная факторизация](#)
6. [Нейросети](#)
7. [Learning theory](#)
8. [Обучение ранжированию](#)
9. [Обучение без учителя](#)


## Практические темы
- [Оценка качества работы алгоритмов и метрики](#)
- [Подбор гиперпараметров](#)
- [Предобработка данных](#)
- [Отбор признаков](#)
- [Визуализация данных](#)
- [Полезные библиотеки и инструменты](#)


#### [Глоссарий используемых терминов](./GLOSSARY.md)

<hline />
<em>
Если вы нашли неточность, сообщите о ней, а лучше опубликуйте пулл-реквест с фиксом :)
<br/><br/>
© Школа анализа данных, 2020
</em>

